# Cpp-Dialogue-RPG-Game

Terminálová dialogová hra psaná procedurálně v C++. Projekt slouží jako ukázka jednoduché kombinace elementárních prvků algoritmu na ukázku studentům programování.

## Obsah repozitáře
- Soubor `Dudek_CPlusPLusRPG.cpp` obsahuje zdrojový kód samotné hry, psané v C++.
- Soubor `Dudek_CPlusPLusRPG.exe` je samostatným spustitelným souborem s terminálovým oknem aplikace. Pro spuštění aplikace je třeba stáhnout si celý repozitář a mít správně nastavení proměnné prostředí s kompilátorem GCC (MinGW).

### Nastavení prostředí s kompilátorem pro Windows

1. Pokud nemáte, stáhněte a nainstalujte si balíček s kompilátorem MinGW z adresy: https://sourceforge.net/projects/mingw/files/latest/download
2. Otevřte si vlastnosti pro "Tento Počítač"
3. Vyberte "Upřesnit nastavení systému"
4. Vyberte poslední položku "Proměnné prostředí"
5. U spodní části "Systémové prostředí" vyberte ze seznamu položku "Path" a vyberte akci "Upravit"
6. Přidejte nový záznam, který odkazuje na Vaše umístění do adresáře `bin` instalovaného kompilátoru. Např. pro MinGW je to: `"C:\MinGW\bin"` V případě instalovaného Code::Blocks IDE (pro studenty) je to pro 64bit OS `"C:\Program Files\CodeBlocks\MinGW\bin"` a pro 32bit OS je to `"C:\Program Files (x86)\CodeBlocks\MinGW\bin"`
7. Vše potvrďte OK

Pro ověření zda jste správně nastavili proměnné prostředí pro běh kompilátoru si otevřete příkazový řádek CMD a zadejte příkaz:
`gcc --version`

## Poznámky a připomínky
Jakékoliv návrhy na vylepšení hry nebo bugy zadávejte pomocí issues: https://gitlab.com/michaldudekit/cpp-dialogue-rpg-game/-/issues
