#include <iostream>
#include <stdlib.h>
using namespace std;

int utok(int profese){
    int utok;
    if (profese == 1){
        utok = (rand() % 15);
    } else if (profese == 2){
        utok = rand() % 20;
    } else if (profese == 3){
        utok = rand() % 12;
    } else if (profese == 4){
        utok = rand() % 10;
    }
    return utok;
}
int odpocinek(int profese){
    int odpocinek;
    if (profese == 1){
        odpocinek = rand() % 10;
    } else if (profese == 2){
        odpocinek = rand() % 15;
    } else if (profese == 3){
        odpocinek = rand() % 20;
    } else if (profese == 4){
        odpocinek = rand() % 15;
    }
    return odpocinek;
}
int uzdraveni(int profese){
    int uzdraveni;
    if (profese == 1){
        uzdraveni = rand() % 8;
    } else if (profese == 2){
        uzdraveni = rand() % 8;
    } else if (profese == 3){
        uzdraveni = rand() % 12;
    } else if (profese == 4){
        uzdraveni = rand() % 25;
    }
    return uzdraveni;
}
int vycerpani(int profese){
    int vycerpani;
    if (profese == 1){
        vycerpani = (rand() % 15);
    } else if (profese == 2){
        vycerpani = rand() % 20;
    } else if (profese == 3){
        vycerpani = rand() % 18;
    } else if (profese == 4){
        vycerpani = rand() % 22;
    }
    return vycerpani;
}
void vyhra(int hb, int pcb, string nickname){
    if (hb > pcb){
        cout << "Hrac " << nickname << " VYHRAVA! A PC prohral!" << endl;
        cout << "\tHrac: " << hb << endl;
        cout << "\tPC: " << pcb << endl;
    } else if (pcb > hb){
        cout << "PC VYHRAVA! A hrac " << nickname << " prohral!" << endl;
        cout << "\tPC: " << pcb << endl;
        cout << "\tHrac: " << hb << endl;
    }
}


int main(){
    string hraci[4][3] = {{"Warrior","120","100"},{"Mage","100","100"},{"Shaman","90","110"},{"Priest","85","120"}};
    int profese, menu;
    bool checker = 0;
    string nickname;

    cout << "#### Vitejte v soubojove hre, kde stesti hraje velkou roli. ###" << endl << endl;


    do {
        cout << "Vyberte si profesi [cislo 1-4]: " << endl;
        for (int i = 0; i < 4; i++){
            cout << i+1 << ". " << hraci[i][0] << "; ";
        }
        cin >> profese;
        if (profese > 0 && profese <= 4){
            checker = 1;
        } else {
            checker = 0;
            cout << "Nebylo zadano spravne cislo!" << endl;
        }
    } while (checker == 0);

    cout << "Zadejte jmeno sve postave: ";
    cin >> nickname;

    cout << "Tva postava je: " << endl;
    cout << "\t" << "Jmeno: " << nickname << endl;
    cout << "\t" << "Povolani: " << hraci[profese-1][0] << endl;
    cout << "\t" << "Zivoty: " << hraci[profese-1][1] << endl;
    cout << "\t" << "Energie: " << hraci[profese-1][2] << endl;

    do {
        cout << endl << "######## Menu ########" << endl;
        cout << "1. Zacit novy souboj proti PC" << endl;
        cout << "2. Ukoncit hru" << endl;
        cout << ": ";
        cin >> menu;

        if (menu == 1){
                char zacatek;
                int akce, pc = (rand() % 4), tahy;
                cout << "[PC] Vybiram profesi: " << hraci[pc-1][0] << endl;
                cout << "[PC] Chces zacit? [a/n]: ";
                cin >> zacatek;
                cout << "Zadej maximalni pocet tahu: ";
                cin >> tahy;
                cout << "[HRA] Generuji nove hraci pole..." << endl;

                int ha,hb,hc;
                int pca,pcb,pcc;

                if (hraci[profese-1][0] == "Warrior"){
                    ha = 1;
                } else if (hraci[profese-1][0] == "Mage"){
                    ha = 2;
                } else if (hraci[profese-1][0] == "Shaman"){
                    ha = 3;
                } else if (hraci[profese-1][0] == "Priest"){
                    ha = 4;
                }
                hb = stoi(hraci[profese-1][1]);
                hc = stoi(hraci[profese-1][2]);

                if (hraci[pc-1][0] == "Warrior"){
                    pca = 1;
                } else if (hraci[pc-1][0] == "Mage"){
                    pca = 2;
                } else if (hraci[pc-1][0] == "Shaman"){
                    pca = 3;
                } else if (hraci[pc-1][0] == "Priest"){
                    pca = 4;
                }
                pcb = stoi(hraci[pc-1][1]);
                pcc = stoi(hraci[pc-1][2]);

                int atributyHrac[3] = {ha,hb,hc};
                int atributyPc[3] = {pca,pcb,pcc};

                cout << "## Checking players: " << endl;
                cout << "Hrac: " << endl;
                cout << "Profese: " << ha << ", Zivoty: " << hb << ", Energie: " << hc << endl;
                cout << "Pocitac: " << endl;
                cout << "Profese: " << pca << ", Zivoty: " << pcb << ", Energie: " << pcc << endl;


                if (zacatek == 'a'){
                    cout << "Zacina hrac jmenem: " << nickname;
                    do {
                        cout << endl << "Vyber si akci: 1. utok; 2. odpocinek; 3. uzdraveni: ";
                        cin >> akce;
                        cout << endl;

                        if (akce == 1){
                            // utok();
                            if (hc >= 10){
                                pcb = pcb-utok(ha);
                                cout << "Hrac jmenem " << nickname << " zautocil! A PC zbyva " << pcb << " zivotu.";
                                hc = hc - vycerpani(ha);
                                cout << " Hraci " << nickname << " zbyva " << hc << " energie." << endl;
                            } else {
                                cout << "Hrac " << nickname << " nemas dostatek energie na utok, mel bys odpocivat." << endl;
                            }

                            if (pcc >= 10){
                                hb = hb - utok(pca);
                                cout << "PC zautocil! A hraci " << nickname << " zbyva " << hb << " zivotu.";
                                pcc = pcc - vycerpani(pca);
                                cout << " PC zbyva " << pcc << " energie." << endl;
                            } else {
                                cout << "PC nema dostatek energie na protiutok." << endl;
                            }

                            tahy--;
                        } else if (akce == 2){
                            // odpocinek();
                            int odpo = odpocinek(ha);
                            hc = hc + odpo;
                            cout << "Hrac jmenem " << nickname << " si odpocinul a ziskava: " << odpo << " odpocinek." << endl;

                            if ((rand() % 10) > 4){
                                if (pcc >= 10){
                                    hb = hb - utok(pca);
                                    cout << "PC zautocil! A hraci " << nickname << " zbyva " << hb << " zivotu.";
                                    pcc = pcc - vycerpani(pca);
                                    cout << "PC zbyva " << pcc << " energie." << endl;
                                } else {
                                    cout << "PC nema dostatek energie na utok." << endl;
                                }
                            } else {
                                int odpoPC = odpocinek(pca);
                                pcc = pcc + odpoPC;
                                cout << "PC si take odpocinul a ziskava: " << odpoPC << " odpocinek." << endl;
                            }

                            tahy--;
                        } else if (akce == 3){
                            // uzdraveni();
                            int uzdrav = uzdraveni(ha);
                            hb = hb + uzdrav;
                            cout << "Hrac jmenem " << nickname << " se uzdravil. A ziskava:" << uzdrav << " zivotu." << endl;

                            if ((rand() % 10) > 4){
                                if (pcc >= 10){
                                    hb = hb - utok(pca);
                                    cout << "PC zautocil! A hraci " << nickname << " zbyva " << hb << " zivotu.";
                                    pcc = pcc - vycerpani(pca);
                                    cout << "PC zbyva " << pcc << " energie." << endl;
                                } else {
                                    cout << "PC nema dostatek energie na utok." << endl;
                                }
                            } else {
                                int uzdravPC = uzdraveni(pca);
                                pcb = pcb + uzdravPC;
                                cout << "PC se take uzdravil a ziskava: " << uzdravPC << " zivotu." << endl;
                            }

                            tahy--;
                       } else {
                            cout << "Ha! Spatne cislo, promarnil jsi svuj tah!" << endl;

                            if ((rand() % 10) > 4){
                                if (pcc >= 10){
                                    hb = hb - utok(pca);
                                    cout << "PC zautocil! A hraci " << nickname << " zbyva " << hb << " zivotu.";
                                    pcc = pcc - vycerpani(pca);
                                    cout << "PC zbyva " << pcc << " energie." << endl;
                                } else {
                                    cout << "PC nema dostatek energie na utok. To mas, ale stesti!" << endl;
                                }
                            }

                            tahy--;
                        }

                        // Kontrola zivotu
                        if (hb <= 0 || pcb <=0){
                            vyhra(hb,pcb,nickname);
                            tahy = 0;
                        }

                        // Kontrola vyhry
                        if (tahy <= 0){
                            vyhra(hb,pcb,nickname);
                            tahy = 0;
                        }

                    } while (tahy > 0);

                } else if (zacatek == 'n'){
                    cout << "Zacina hrac PC";
                    do {
                        cout << endl << "Vyber si akci: 1. utok; 2. odpocinek; 3. uzdraveni: ";
                        cin >> akce;
                        cout << endl;

                        if (akce == 1){
                            // utok();
                            if (pcc >= 10){
                                hb = hb - utok(pca);
                                cout << "PC zautocil! A hraci " << nickname << " zbyva " << hb << " zivotu.";
                                pcc = pcc - vycerpani(pca);
                                cout << " PC zbyva " << pcc << " energie." << endl;
                            } else {
                                cout << "PC nema dostatek energie na protiutok." << endl;
                            }

                            if (hc >= 10){
                                pcb = pcb-utok(ha);
                                cout << "Hrac jmenem " << nickname << " zautocil! A PC zbyva " << pcb << " zivotu.";
                                hc = hc - vycerpani(ha);
                                cout << " Hraci " << nickname << " zbyva " << hc << " energie." << endl;
                            } else {
                                cout << "Hrac " << nickname << " nemas dostatek energie na utok, mel bys odpocivat." << endl;
                            }

                            tahy--;
                        } else if (akce == 2){
                            // odpocinek();
                            int odpo = odpocinek(ha);
                            hc = hc + odpo;
                            cout << "Hrac jmenem " << nickname << " si odpocinul a ziskava: " << odpo << " odpocinek." << endl;

                            if ((rand() % 10) > 4){
                                if (pcc >= 10){
                                    hb = hb - utok(pca);
                                    cout << "PC zautocil! A hraci " << nickname << " zbyva " << hb << " zivotu.";
                                    pcc = pcc - vycerpani(pca);
                                    cout << "PC zbyva " << pcc << " energie." << endl;
                                } else {
                                    cout << "PC nema dostatek energie na utok." << endl;
                                }
                            } else {
                                int odpoPC = odpocinek(pca);
                                pcc = pcc + odpoPC;
                                cout << "PC si take odpocinul a ziskava: " << odpoPC << " odpocinek." << endl;
                            }

                            tahy--;
                        } else if (akce == 3){
                            // uzdraveni();
                            int uzdrav = uzdraveni(ha);
                            hb = hb + uzdrav;
                            cout << "Hrac jmenem " << nickname << " se uzdravil. A ziskava:" << uzdrav << " zivotu." << endl;

                            if ((rand() % 10) > 4){
                                if (pcc >= 10){
                                    hb = hb - utok(pca);
                                    cout << "PC zautocil! A hraci " << nickname << " zbyva " << hb << " zivotu.";
                                    pcc = pcc - vycerpani(pca);
                                    cout << "PC zbyva " << pcc << " energie." << endl;
                                } else {
                                    cout << "PC nema dostatek energie na utok." << endl;
                                }
                            } else {
                                int uzdravPC = uzdraveni(pca);
                                pcb = pcb + uzdravPC;
                                cout << "PC se take uzdravil a ziskava: " << uzdravPC << " zivotu." << endl;
                            }

                            tahy--;
                       } else {
                            cout << "Ha! Spatne cislo, promarnil jsi svuj tah!" << endl;

                            if ((rand() % 10) > 4){
                                if (pcc >= 10){
                                    hb = hb - utok(pca);
                                    cout << "PC zautocil! A hraci " << nickname << " zbyva " << hb << " zivotu.";
                                    pcc = pcc - vycerpani(pca);
                                    cout << "PC zbyva " << pcc << " energie." << endl;
                                } else {
                                    cout << "PC nema dostatek energie na utok. To mas, ale stesti!" << endl;
                                }
                            }

                            tahy--;
                        }

                        // Kontrola zivotu
                        if (hb <= 0 || pcb <=0){
                            vyhra(hb,pcb,nickname);
                            tahy = 0;
                        }

                        // Kontrola vyhry
                        if (tahy <= 0){
                            vyhra(hb,pcb,nickname);
                            tahy = 0;
                        }

                    } while (tahy > 0);


                } else {
                    cout << "Nene, takhle by to neslo, musis vybrat spravne 'a' nebo 'n'!";
                    checker = 0;
                }

        } else if (menu == 2){
            checker = 0;
            cout << endl << "Dekujeme za vyuziti teto hry!" << endl;
            system("pause");
        }

    } while (checker != 0);
}
